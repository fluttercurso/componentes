import 'package:flutter/material.dart';

class HomePageTemp extends StatelessWidget {
  final opciones = [
    'juan',
    'kelly',
    'guillermo',
    'don luis',
    'bebe toca bolas',
    'uno',
    'dos',
    'tres',
    'cuatro',
    'cinco',
    'seis',
    'siete',
    'casa',
    'cama',
    'comida',
    'frijoles',
    'morir'
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Componentes temp'),
        elevation: 5.5,
      ),
      body: ListView(
        children: _crearItemsCorto(),
      ),
    );
  }

  List<Widget> _crearItems() {
    List<Widget> lista = new List<Widget>();

    for (String opt in opciones) {
      final tempWidget = ListTile(
        title: Text(opt),
      );
      lista.add(tempWidget);
      lista.add(Divider());
    }

    return lista;
  }

  List<Widget> _crearItemsCorto() {
    return opciones.map((item2) => _construirComponentes(item: item2)).toList();
  }

  Column _construirComponentes({String item}) {
    return Column(
      children: [
        ListTile(
          title: Text(item),
          subtitle: Text('Subtitulo'),
          leading: Icon(Icons.access_alarm_rounded),
          trailing: Icon(Icons.arrow_back),
          onTap: () {},
        ),
        Divider(
          color: Color.fromRGBO(39, 9, 248, 1),
        )
      ],
    );
  }
}
